FROM golang:alpine AS builder

WORKDIR /app

ADD . .

RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o override

FROM alpine:latest

COPY --from=builder /app/override /usr/local/bin/override

RUN sed -i 's/dl-cdn.alpinelinux.org/mirrors.aliyun.com/g' /etc/apk/repositories

RUN apk --no-cache add tzdata

WORKDIR /app

ENTRYPOINT ["/usr/local/bin/override"]

EXPOSE 8181
